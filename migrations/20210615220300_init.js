exports.up = (knex) =>
  knex.schema
    .createTable('users', (table) => {
      table.increments('id');
      table.boolean('registered');
      table.boolean('confirmed');
      table.string('name', 255);
      table.string('username', 255);
      table.string('emailAddress', 255);
      table.string('password', 255);
      table.dateTime('createdAt');
      table.dateTime('updatedAt');
    })
    .createTable('item', (table) => {
      table.increments('id');
      table.integer('item');
      table.dateTime('someDate');
      table.dateTime('createdAt');
      table.dateTime('updatedAt');
    });

exports.down = (knex) => knex.schema.dropTable('users').dropTable('item');

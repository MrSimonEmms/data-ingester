FROM node:14-alpine
WORKDIR /opt/app
COPY dist dist
COPY package.json .
COPY package-lock.json .
RUN npm prune --production
ENV DATA_DIR=/data
VOLUME [ "/data" ]
USER node
CMD [ "npm", "start" ]

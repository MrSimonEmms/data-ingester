# Data Ingester

> Deprecated in favour of [Gobblr](https://github.com/mrsimonemms/gobblr)

This ingests test data into the development stack. Not for production use.

## Environment Variables

| Environment Variable | Required | Default | Description |
| --- | --- | --- | --- |
| DATA_DIR | Y | `/data` | Path to the data directory |
| DB_TYPE | Y | - | Driver to use |
| MAX_TRIES | N | `1` | Maximum number of attempts. In docker compose, this can be replaced by `restart: on-failure` |
| TIMEOUT | N | `1000` | Gap in milliseconds between each attempt. Delay is calculated by `attemptNumber * timeout` formula |
| LOGGER_LEVEL | N | `info` | Log level |
| SERVER_PORT | N | `3000` | Port to listen on |

> If you are using a database that expects the table structure to be defined (eg, MySQL),
> you must execute that outside of this. Most database drivers will do that for you, for
> example in [TypeORM](https://typeorm.io/#/migrations) or
> [Sequelize](https://sequelize.org/master/manual/migrations.html)

## Folder structure

### Data

> **tl;dr** create files in the format `/data/<int>-<collection>.<js/json>`

This is where the data lives. You can store data either in JSON or JS format
- JSON is static whereas JS can be used if you need to link to other data or
calculate things.

The file format **MUST** be `<int>-<collection>`, eg `001-user.json`. The number
is the position in which it's run (eg, `001` is run before `002`) - this allows
you to link through to data already in a data table (useful for linking foreign
keys in SQL DBs).

The `collection` name is the name of the collection/table - in the above
example, it will be called `user`.

#### JSON

This can be a simple array of objects. If you use an array of objects, it will
add a `createdAt` and `updatedAt` key with the datetime of when it was created.

```json
[{...}] // Insert the data here
```

If you wish not to have the created/updated metadata in, you can use this format:

```json
{
  "meta": {
    "createdKey": "created", // Define the name of the created key
    "created": false, // Add "created" metadata
    "updatedKey": "updated", // Define the name of the updated key
    "updated": false // Add "updated" metadata
  },
  "data": [{...}] // Insert the data here
}
```

#### JS

You can run any NodeJS script in here. It must return a function which returns
an array of objects from `module.exports` in the same format as JSON.

```js
module.exports = () => [{...}] // Insert the data here
```

As with the JSON, you can add the `meta` object if you wish to manipulate the
created or updated data.

### Drivers

This exists to allow for future database types to be added. As an interface,
the driver must have `auth`, `close`, `insertBulk` and `truncate` methods
defined.

#### MongoDB

##### Environment Variables

| Environment Variable | Required | Default | Description |
| --- | --- | --- | --- |
| MONGODB_URL | Y | - | The MongoDB URL, eg `mongodb://mongodb:27017/auth` |

#### MySQL

##### Environment Variables

> `DB_TYPE` must be set to `sql` and `SQL_CLIENT` must be set to `mysql`

| Environment Variable | Required | Default | Description |
| --- | --- | --- | --- |
| MYSQL_DATABASE | Y | - | The database name, eg `ingester` |
| MYSQL_HOST | N | `localhost` | The database host, eg `localhost` |
| MYSQL_PASSWORD | N | - | The database password |
| MYSQL_PORT | N | `3306` | The database name |
| MYSQL_USER | NY | - | The database user |

#### PostgreSQL

##### Environment Variables

> `DB_TYPE` must be set to `sql` and `SQL_CLIENT` must be set to `pg`

| Environment Variable | Required | Default | Description |
| --- | --- | --- | --- |
| PGSQL_DATABASE | Y | - | The database name, eg `ingester` |
| PGSQL_HOST | N | `localhost` | The database host, eg `localhost` |
| PGSQL_PASSWORD | N | - | The database password |
| PGSQL_PORT | N | `5432` | The database name |
| PGSQL_USER | NY | - | The database user |

#### SQLite3

##### Environment Variables

> `DB_TYPE` must be set to `sql` and `SQL_CLIENT` must be set to `sqlite3`

| Environment Variable | Required | Default | Description |
| --- | --- | --- | --- |
| SQLITE3_FILENAME | Y | - | The path to the SQLite3 file, eg `/path/to/db.sql` |

## Reloading Data

When running end-to-end tests, it is a good idea to reset the data between each
test run. This ensures that any data changed in a test is not persisted and ensures
that tests can be run in any order.

To reset the data, call `POST:/data/reset`. This endpoint receives no `body` and
returns the data that was ingested:

```ts
interface Response {
  [table: string]: Object[]
}
```

HTTP is used for this to make this service not dependent upon any language or framework.
As an example, to get this to reset in the [Jest](https://jestjs.io) framework, ensure
that this exists outside a `describe` block:

```js
const axios = require('axios');

beforeEach(() => axios.post('http://data-ingest:3000/data/reset'));
```

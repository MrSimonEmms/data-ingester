// Used to control migrations in example

const connection = {
  mysql: {
    database: process.env.MYSQL_DATABASE,
    host: process.env.MYSQL_HOST ?? 'localhost',
    password: process.env.MYSQL_PASSWORD,
    port: Number(process.env.MYSQL_PORT ?? 3306),
    user: process.env.MYSQL_USER,
  },
  pg: {
    database: process.env.PGSQL_DATABASE,
    host: process.env.PGSQL_HOST ?? 'localhost',
    password: process.env.PGSQL_PASSWORD,
    port: Number(process.env.PGSQL_PORT ?? 5432),
    user: process.env.PGSQL_USER,
  },
  sqlite3: {
    filename: process.env.SQLITE3_FILENAME,
  },
};

const client = process.env.SQL_CLIENT;

module.exports = {
  development: {
    client,
    connection: connection[client],
  },
};

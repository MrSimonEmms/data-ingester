import * as path from 'path';

export default {
  data: `${process.env.DATA_DIR ?? path.join(__dirname, '..', 'data')}/**/*.{js,json}`,
  driver: process.env.DB_TYPE?.toLowerCase(),
  maxTries: Number(process.env.MAX_TRIES ?? 1),
  timeout: Number(process.env.TIMEOUT ?? 1000),
  drivers: {
    mongodb: {
      url: process.env.MONGODB_URL,
      opts: {
        useUnifiedTopology: true,
      },
    },
    sql: {
      client: process.env.SQL_CLIENT,
      mysql: {
        database: process.env.MYSQL_DATABASE,
        host: process.env.MYSQL_HOST ?? 'localhost',
        password: process.env.MYSQL_PASSWORD,
        port: Number(process.env.MYSQL_PORT ?? 3306),
        user: process.env.MYSQL_USER,
      },
      pg: {
        database: process.env.PGSQL_DATABASE,
        host: process.env.PGSQL_HOST ?? 'localhost',
        password: process.env.PGSQL_PASSWORD,
        port: Number(process.env.PGSQL_PORT ?? 5432),
        user: process.env.PGSQL_USER,
      },
      sqlite3: {
        filename: process.env.SQLITE3_FILENAME,
      },
    },
  },
  logger: {
    level: process.env.LOGGER_LEVEL ?? 'info',
  },
  server: {
    host: '0.0.0.0',
    port: Number(process.env.SERVER_PORT ?? 3000),
  },
};

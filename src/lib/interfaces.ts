export interface IDriver {
  auth(): Promise<void>;
  close(): Promise<void>;
  insertBulk(collection: string, data: Object[]): Promise<number>;
  truncate(name: string): Promise<void>;
}

export interface IIngestOutput {
  [key: string]: Object;
}

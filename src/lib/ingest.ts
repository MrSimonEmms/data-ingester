import * as path from 'path';
import { promises as fs } from 'fs';
import { FastifyLoggerInstance } from 'fastify';
import * as glob from 'glob';
import config from '../config';
import { IDriver, IIngestOutput } from './interfaces';

async function getConnection(): Promise<IDriver> {
  const { driver } = config;

  const { default: Driver } = await import(`../drivers/${driver}`);

  const db = new Driver();

  await db.auth();

  return db;
}

async function getFiles(): Promise<string[]> {
  return new Promise((resolve, reject) => {
    glob(config.data, { nosort: true }, (err, files) => {
      if (err) {
        reject(err);
        return;
      }

      resolve(files);
    });
  }).then((files: string[]) =>
    files.sort((a, b) => {
      /* Always put link tables to end - assume they've got keys */
      if (a.includes('link_')) {
        return 1;
      }
      if (b.includes('link_')) {
        return -1;
      }
      if (a < b) {
        return -1;
      }
      if (a > b) {
        return 1;
      }

      return 0;
    }),
  );
}

async function loadFile(file: string): Promise<any> {
  const extension = path.extname(file)?.toLowerCase();
  if (extension === '.json') {
    return JSON.parse(await fs.readFile(file, 'utf8'));
  }
  if (extension === '.js') {
    const script = await import(file);

    if (typeof script !== 'function') {
      throw new Error('JS scripts must be a function');
    }

    return script();
  }

  throw new Error(`Unknown extension type: ${extension}`);
}

export default async (logger: FastifyLoggerInstance): Promise<IIngestOutput> => {
  logger.debug('Creating connection to database');
  const connection = await getConnection();

  const files = await getFiles();

  const output: IIngestOutput = {};

  await files.reduce(
    (thenable, file) =>
      thenable.then(async () => {
        logger.info({ file }, 'Loading file from disk');
        const items = await loadFile(file);

        /* Remove any digits at the start of the file */
        const table = path.basename(path.basename(file, '.js'), '.json').replace(/^(\d.*-)/, '');

        const { meta = {}, data = items } = items;

        if (!Array.isArray(data)) {
          throw new Error(`Data not an array: ${table}`);
        }

        if (data.length === 0) {
          logger.warn({ table }, 'No data in file');
          return;
        }

        logger.info({ table }, 'Truncating table');

        /* Clear out any existing data */
        await connection.truncate(table);

        const parsedData = data.map((item) => {
          const now = new Date();
          if (!item.createdAt && meta.created !== false) {
            Object.defineProperty(item, meta.createdKey ?? 'createdAt', {
              enumerable: true,
              value: now,
            });
          }
          if (!item.updatedAt && meta.updated !== false) {
            Object.defineProperty(item, meta.updatedKey ?? 'updatedAt', {
              enumerable: true,
              value: now,
            });
          }

          return item;
        });

        try {
          const inserts = await connection.insertBulk(table, parsedData);

          logger.info({ rows: inserts, table }, 'Inserted data');

          output[table] = parsedData;
        } catch (err) {
          logger.warn({ err }, 'Failed to ingest data - truncating');

          try {
            await connection.truncate(table);
          } catch (truncateError) {
            logger.error({ err: truncateError }, 'Error truncating data');

            throw truncateError;
          }

          throw err;
        }
      }),
    Promise.resolve(),
  );

  logger.debug('Closing database connection');

  await connection.close();

  return output;
};

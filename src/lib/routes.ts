import { FastifyInstance } from 'fastify';
import ingest from './ingest';
import { IIngestOutput } from './interfaces';

export default async (app: FastifyInstance) => {
  app.post('/data/reset', async (): Promise<IIngestOutput> => ingest(app.log));
};

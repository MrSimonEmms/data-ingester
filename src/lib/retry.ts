import { FastifyLoggerInstance } from 'fastify';

export default (
  maxAttempts: number,
  timeout: number,
  logger: FastifyLoggerInstance,
  fn: () => Promise<unknown>,
  ensureRun: boolean = true,
) => {
  const tasks: (() => Promise<boolean>)[] = [];

  for (let attempt = 1; attempt <= maxAttempts; attempt += 1) {
    tasks.push(async () => {
      logger.info({ attempt }, 'Attempting to ingest data');

      try {
        await fn();
        logger.info({ attempt }, 'Successfully executed on attempt');

        return true;
      } catch (err) {
        logger.warn({ attempt, err }, 'Attempt failed');

        if (attempt >= maxAttempts) {
          logger.error('No retries left');
          throw err;
        }

        const delay = timeout * attempt;
        logger.info({ delay: delay / 1000 }, 'Retrying after delay');
        await new Promise((resolve) => setTimeout(resolve, delay));
      }

      return false;
    });
  }

  if (tasks.length === 0 && ensureRun) {
    throw new Error('No attempts tried');
  }

  let exited = false;
  return tasks.reduce(
    (thenable, task) =>
      thenable.then(() => {
        if (exited) {
          return thenable;
        }

        return thenable.then(async () => {
          exited = await task();
        });
      }),
    Promise.resolve(),
  );
};

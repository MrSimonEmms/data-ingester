import knex, { Knex } from 'knex';
import { IDriver } from '../lib/interfaces';
import config from '../config';

export default class SQL implements IDriver {
  private db: Knex;

  async auth() {
    const { client } = config.drivers.sql;
    const connection = config.drivers.sql[client];

    this.db = knex({
      client,
      connection,
    });

    /* Do a dummy query to check connection ok */
    await this.db.raw('SELECT 1 + 1 AS result');
  }

  async close() {
    await this.db.destroy();
  }

  async insertBulk(name, data) {
    await this.db.table(name).insert(data);

    return data.length;
  }

  async truncate(name) {
    await this.db.table(name).truncate();
  }
}

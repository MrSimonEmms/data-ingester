import { MongoClient } from 'mongodb';
import * as uuid from 'uuid';
import { IDriver } from '../lib/interfaces';
import config from '../config';

export default class MongoDB implements IDriver {
  private db: MongoClient;

  private opts = config.drivers.mongodb;

  async auth() {
    this.db = new MongoClient(this.opts.url, this.opts.opts);
    await this.db.connect();

    /* Do a dummy query to check connection ok */
    await this.db.db('admin').command({ ping: 1 });
  }

  async close() {
    await this.db.close();
  }

  async insertBulk(collection, data) {
    const { insertedCount } = await this.db
      .db()
      .collection(collection)
      .insertMany(
        data.map((item) => {
          // eslint-disable-next-line no-underscore-dangle
          if (!item._id) {
            Object.defineProperty(item, '_id', {
              enumerable: true,
              value: uuid.v4(),
            });
          }
          return item;
        }),
      );
    return insertedCount;
  }

  async truncate(name: string) {
    try {
      await this.db.db().collection(name).drop();
    } catch (err) {
      /* Errors if collection doesn't exist */
      if (err.codeName !== 'NamespaceNotFound') {
        throw err;
      }
    }
  }
}

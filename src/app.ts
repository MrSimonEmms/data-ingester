/**
 * App
 *
 * This is designed to be used as a standalone application
 */

import fastify, { FastifyInstance } from 'fastify';
import config from './config';
import routes from './lib/routes';
import ingest from './lib/ingest';
import retry from './lib/retry';

async function main(app: FastifyInstance): Promise<void> {
  app.log.info({ config }, 'Loading');

  await retry(config.maxTries, config.timeout, app.log, () => ingest(app.log));

  app.register(routes);

  await app.listen(config.server.port, config.server.host);

  app.log.info({ config }, 'Server started');
}

const app = fastify({
  logger: {
    level: config.logger.level,
  },
});

main(app).catch((err) => {
  app.log.error({ err }, 'Failed to start server');
  process.exit(1);
});
